module.exports = {
    enabled: true,
    migrations: { tableName: 'knex_migrations' },
    seeds: { tableName: './seeds' },

    client: 'mysql',
    connection: {

        host: process.env.MYSQL_HOST || 'localhost',

        user: process.env.MYSQL_USER || 'user',
        password: process.env.MYSQL_PASSWORD || 'password',

        database: process.env.MYSQL_DATABASE || 'test',
        port: process.env.MYSQL_PORT || '3306',
        
        charset: 'utf8',

    },
    pool: {
        min: 0, max: 7
    }
};