const jwt = require('jsonwebtoken');
const Boom = require('boom');

module.exports = function (server, options) {
	return {
		authenticate: function (request, h) {
			if (typeof options.privateKey === 'undefined' || !options.privateKey) {
				throw Boom.unauthorized('Authentication not set', 'jwt');
			}

			const req = request.raw.req;
			const authorization = req.headers.authorization || null;
			if (!authorization) {
				throw Boom.unauthorized("Authorization not set");
			}
			const authArray = authorization.split(' ');
			const bearer = authArray[0];
			const token = authArray[1];

			if (bearer.toLowerCase() !== "bearer") {
				throw Boom.unauthorized('Authentication scheme not correctly set');	
			}
			
			const verify = jwt.verify(token, options.privateKey, function (err, decoded) {
				// verify other things in the payload, not just username
				if (!err && typeof decoded.email !== 'undefined' && typeof decoded.uuid !== 'undefined') {
					// get scope
					return decoded;
				}
				return false;
			});

			if (verify) {
				return h.authenticated({ credentials: verify });
			}

			throw Boom.unauthorized('Not Authorized', 'jwt');
		}
	};

}
