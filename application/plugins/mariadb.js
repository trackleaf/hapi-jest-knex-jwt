const knex = require('knex');

module.exports = {
    name: 'mariadb',
    version: '1.0.0',
    register: function (server, options) {
        server.app.db = knex(options);
    },
    once: true
};
