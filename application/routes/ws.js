const handlers = require('../handlers/base');

module.exports = [
    { 
	    method: 'GET',
	    path: '/us',
	    config: { 
		id: 'user-status',
		handler: function (request, h) {
			return 'user updated profile';
		}
	    }
    }
];
