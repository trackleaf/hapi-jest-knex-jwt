const handlers = require('../handlers/base');

module.exports = [
    { 
	    method: 'GET',
	    path: '/admin/users',
	    config: { 
		auth: {
			mode: 'required',
			strategy: 'jwt',
			access: {
				scope: ['+admin']
			}
		},
		handler: handlers.admin.users
	    }
    }
];
