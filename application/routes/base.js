// load the routes
module.exports = [].concat(
	require('./admin'),
	require('./sessions'),
	require('./ws')
);
