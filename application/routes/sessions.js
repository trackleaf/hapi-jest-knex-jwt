const handlers = require('../handlers/base');

module.exports = [
    { 
	    method: 'POST',
	    path: '/sessions/create',
	    handler: handlers.sessions.create
    }
];
