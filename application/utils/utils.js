const bcrypt = require('bcrypt');

module.exports = {	
	verifyEmail: function (email) {
		return false;
	},
	verifyPassword: function (password, hash) {
		return bcrypt.compareSync(password, hash);	
	}
};
