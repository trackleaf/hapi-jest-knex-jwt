const Boom = require('boom');
module.exports = {
	getRoles: function () {
		return [
			'admin',
			'editor',
			'user'
		];	
	}
};
