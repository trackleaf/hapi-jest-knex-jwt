const database = require('../config/database')

module.exports = {
    knex: null,
    loadDb: function (connection) {
	    if (!this.knex) {
		this.knex = connection;
	    }
    },
    getAdminUsers: function () {
	return this.knex('users').where('role', 'admin').select('uuid', 'email', 'publicToken');
    },
    getUserPassword: function (email) {
	
    	return this.knex('users').where('email', email).select('password');
    },
    getUserCreds: function (email) {
    	return this.knex('users').where('email', email).select('password', 'email', 'uuid', 'role');
    }
};
