const users_model = require('../models/users');
const jwt = require('jsonwebtoken');
const privateKey = process.env.JWT_PRIVATE_KEY;
const Boom = require('boom');
const Utils = require('../utils/utils');

module.exports = { 
    sessions: {
		create: async function (request, h) { 
			
			try {
				users_model.loadDb(request.server.app.db);
				
				const email = request.payload.email;
				const password = request.payload.password;
				const resultArray = await users_model.getUserCreds(email);

				if (resultArray.length === 0) {
					return Boom.badRequest("User not found");
				}
				
				if (!Utils.verifyPassword(password, resultArray[0].password)) {
					return Boom.badRequest('invalid password');
				}

				const token = jwt.sign({
					email: resultArray[0].email,
					uuid: resultArray[0].uuid,
					scope: resultArray[0].role
				}, privateKey, { expiresIn: '1h' });
				return token;
			} catch (error) {
				return Boom.badRequest(error);
			}
		}
    }
}
