// load the different handlers here
module.exports = Object.assign({},
    require('./admin'),
    require('./sessions')
);
