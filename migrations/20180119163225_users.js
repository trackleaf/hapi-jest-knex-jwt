
exports.up = function(knex, Promise) {
	return Promise.all([
		knex.schema.createTableIfNotExists('users', function (table) {
		table.string('uuid').notNullable();
		table.string('password').notNullable();
		table.string('email').notNullable();
		table.string('first_name');
		table.string('last_name');
		table.boolean('active').defaultTo(1);
		table.string('publicToken');
		table.string('privateToken');
		table.string('role');
		table.timestamps(false, true);
		})
	]);	
  
};

exports.down = function(knex, Promise) {
	return Promise.all([knex.schema.dropTableIfExists('users')]);  
};
