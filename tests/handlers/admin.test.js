const handler = require('../../application/handlers/admin').admin;
 // dummy test :)
jest.mock('../../application/models/users');
const users_model = require('../../application/models/users');

test('users method', function () {
	// setup
	users_model.getAdminUsers = jest.fn().mockReturnValueOnce([1,2,3]);
	users_model.loadDb = jest.fn();
	const requestMock = {
		server: {
			app: {
				db: jest.fn()
			}
		}
	};

	// call
	const result = handler.users(requestMock, {});
	
	// test
	expect(users_model.loadDb.mock.calls.length).toBe(1);
	expect(result.length).toBe(3);
});
