const Nes = require('nes');

var client = new Nes.Client('ws://localhost:3000');

const start = async () => {

    await client.connect();
    const payload = await client.request('user-status');  // Can also request '/us'
    console.log(payload);
};

start();
