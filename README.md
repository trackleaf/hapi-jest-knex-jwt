# HapiJS REST API with JWT authentication

This repo contains a few packages that will kick start your development with Hapi, JWT, Knex and Jest (for tests). I will be adding funtionality as time goes on to support other things like Server Sent Events or WebSockets. This is very raw since it was done in a matter of hours but as time goes on I'll refine structure and code to give anyone who uses this a good starting point.

Install all the dependencies with yarn

```
yarn install
```

The directoy structure is as follows:
```
application\
	acl\
	config\
	handlers\
	models\
	plugins\
	routes\
	utils\
migrations\
seeds\
test\
	handlers\
```

To get a database going to do development with this Rest API I ran this

```
docker run --name some-mariadb -e MYSQL_ROOT_PASSWORD=my-secret-pw -e MYSQL_USER=hapijs -e MYSQL_PASSWORD=pass -d -p '3307:3306' mariadb:latest
```
Note: adjust the port as needed. I had another database using 3306 during the time of this development :)

Here is the init sql code to create the database and grant all privileges to the hapijs user

```
>docker exec -it some-mariadb '/bin/bash'
>mysql -u root -p'my-secret-pw'

CREATE DATABASE hapijs;

GRANT ALL PRIVILEGES ON hapijs . * TO 'hapijs'@'%';
```

To run the migrations to create the table and populate it with data run these two commands

```
MYSQL_USER=hapijs MYSQL_PASSWORD=pass MYSQL_DATABASE=hapijs MYSQL_PORT=3307 knex migrate:latest

MYSQL_USER=hapijs MYSQL_PASSWORD=pass MYSQL_DATABASE=hapijs MYSQL_PORT=3307 knex seed:run

```

To run the application I started the server by passing in some env variables
```
JWT_PRIVATE_KEY=supersecret MYSQL_USER=hapijs MYSQL_PASSWORD=pass MYSQL_DATABASE=hapijs MYSQL_PORT=3307 node server.js
```
