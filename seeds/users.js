const bcrypt = require('bcrypt');
const uuid = require('uuid/v4');
const sjcl = require('sjcl');
const saltRounds = 10;
const defaultAdminPassword = 'Th1s1sTh3D3f@ultP@55w0rd!@#';
const MY_NAMESPACE = 'http://www.example.org/';

function sjclRNG() {
  var rndsIn = sjcl.random.randomWords(4);
  var rndsOut = new Array(16);
  for (var i = 0, r; i < 16; i++) {
    if (i % 4 === 0) r = rndsIn.pop();
    rndsOut[i] = r & 0xff;
    r >>>= 8;
  }
  return rndsOut;
}

exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
	var hash = bcrypt.hashSync(defaultAdminPassword, saltRounds);
      return knex('users').insert([
        { uuid: uuid({ rng: sjclRNG }), email: 'michael@example.org', password: hash, publicToken: uuid({ rng: sjclRNG }), privateToken: uuid({ rng: sjclRNG }), role: 'admin' }
      ]);
    });
};
