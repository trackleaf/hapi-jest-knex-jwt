'use strict';

const Hapi = require('hapi');
const routes = require('./application/routes/base');
const Boom = require('boom');
const scheme = require('./application/plugins/authentication');
const mariadb = require('./application/plugins/mariadb');
const Nes = require('nes');
// set this via command line or from .env file
const privateKey = process.env.JWT_PRIVATE_KEY;
const jwt = require('jsonwebtoken');
const database = require('./application/config/database');

// method to get everything going
async function startServer() {
	const server = new Hapi.Server({ port: 3000, host: 'localhost' });

	// create the auth strategry/scheme -> jwt
	server.auth.scheme('jwt', scheme);
	server.auth.strategy('jwt', 'jwt', { privateKey: privateKey });

	// add the connection to the database. Will be stored as
	// request.server.app.db in the handlers
	await server.register({ plugin: mariadb, options: database });

	// web socket support
	await server.register(Nes);

	server.route(routes);

	try {
		await server.start();
		console.log("Server started at http://localhost:3000");
	} catch (err) {
		console.log(err);
	}
}

// and go
startServer();
